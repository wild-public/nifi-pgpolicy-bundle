package org.apache.nifi.authorization;

import org.apache.nifi.authorization.exception.AuthorizationAccessException;
import org.apache.nifi.authorization.exception.AuthorizerCreationException;
import org.apache.nifi.authorization.exception.AuthorizerDestructionException;

import java.util.Set;

/**
 * @author Vitaliy Borisovskiy
 * @since 01.02.2021
 */
public class PgPolicyProvider implements AccessPolicyProvider {
    @Override
    public Set<AccessPolicy> getAccessPolicies() throws AuthorizationAccessException {
        return null;
    }

    @Override
    public AccessPolicy getAccessPolicy(String s) throws AuthorizationAccessException {
        return null;
    }

    @Override
    public AccessPolicy getAccessPolicy(String s, RequestAction requestAction) throws AuthorizationAccessException {
        return null;
    }

    @Override
    public UserGroupProvider getUserGroupProvider() {
        return null;
    }

    @Override
    public void initialize(AccessPolicyProviderInitializationContext accessPolicyProviderInitializationContext) throws AuthorizerCreationException {

    }

    @Override
    public void onConfigured(AuthorizerConfigurationContext authorizerConfigurationContext) throws AuthorizerCreationException {

    }

    @Override
    public void preDestruction() throws AuthorizerDestructionException {

    }
}
